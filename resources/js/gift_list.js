// Click handler
var ajax;
get('dom:load').push(() => {
    ajax = new Ajax();

    Events.attach(document, 'click', (e) => {
        var target = e.target;

        // Which filter we are showing
        if (target?.name) {
            var sect = target.name.replace(/^filter_/, '');
            var val = target.value;
            switch (target.name) {
            // The filter to display
            case 'filter_what' :
                ['cat','store'].forEach((f) => {
                    if (target.value == f) {
                        DOM.child(`.filter.${f}`).classList.remove('hidden');
                    } else {
                        DOM.child(`.filter.${f}`).classList.add('hidden');
                    }
                });
                if (target.value != 'all') {
                    DOM.child(`#filter_${target.value}_all`).checked = true;
                }
                val = 'all';
                // falls through

            default :
                // A filter
                DOM.children('.gift').forEach((o) => {
                    if (sect == 'all' || val == 'all' || o.classList.contains(`${sect}-${val}`)) {
                        o.classList.remove('hidden');
                    } else {
                        o.classList.add('hidden');
                    }
                });
            }

            // Confirming reservation
        } else if (target.closest('.reserve')) {
            target.closest('article').querySelector('.reserve_details').classList.toggle('hidden');

        // Reserved gift
        } else if (target.closest('.reserve_details')) {
            // Toggle to the saving message
            var msg = target.closest('li').querySelector('.message');
            if (!Input.isDefined(msg.dataset.default)) {
                msg.dataset.default = msg.innerHTML;
            } else {
                msg.innerHTML = msg.dataset.default;
            }
            target.closest('.reserve_details').classList.add('hidden');
            msg.classList.remove('hidden');
            // Make the Ajax request
            var giftId = target.closest('li').dataset.id;
            ajax.request({
                'method': 'POST',
                'url': `/gift/${giftId}/reserve`,
                'gift_id': giftId,
                'success': (retVal, config) => { /* eslint-disable-line no-unused-vars */
                    var gift = DOM.child(`li[data-id="${config.gift_id}"]`);
                    gift.querySelector('.message').innerHTML = '<span><strong>Thank you!</strong> We thank you for your generosity!</span>';
                    window.setTimeout(() => {
                        gift.classList.add('fade');
                        window.setTimeout(() => { gift.classList.add('hidden'); }, 750);
                    }, 5000);
                },
                'failure': (xmlHTTP, config) => {
                    var err = xmlHTTP.responseText;
                    if (!Input.isDefined(err) || !err.match(/^[01]/)) {
                        err = '0::Sorry, someting went wrong. Please try again later.';
                    }
                    var errDetails = err.split('::');

                    var msg = document.querySelector(`li[data-id="${config.gift_id}"] .message`);
                    msg.innerHTML = `<span><strong>Oops</strong>: ${errDetails[1]}</span>`;
                    msg.classList.add('error');
                }
            });
        }
    });
});
