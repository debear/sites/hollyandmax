#!/bin/bash
# Locally run the SAST analysers using equivalent docker images

dir_base=$(dirname $0)
dir_root=$(realpath $dir_base/../..)
dir_scripts=$(realpath $dir_base/../_scripts)
dir_artifact="$dir_base/runs/emulated"

. $dir_scripts/helpers.sh # General helpers
. $dir_base/helpers.sh # Specific SAST helpers

# Our job:analyser mapping
declare -A docker_map
docker_map['nodejs-scan-sast']='nodejs-scan'
docker_map['phpcs-security-audit-sast']='phpcs-security-audit'
docker_map['semgrep-sast']='semgrep'

repo=$(basename $dir_root)
display_title "Local SAST emulator for repo '$repo'"

# Identify the last completed run we need to determine the analysers to use
last_run=$(ls -td $dir_base/runs/20??-??-??_* 2>/dev/null | head -1)
if [ -z "$last_run" ]
then
    display_error "To know which analysers to process, this script requires at least one downloaded SAST report from GitLab.com"
    display_info "Please run '$dir_base/report latest' to get the latest SAST report"
    exit 1
fi

# Determine the analysers to use
analysers=$(ls -tr $last_run/*.json.gz | awk -F'/' '{print $NF}' | sed 's/\.json\.gz$//' | grep -Fv 'jobs')
filters="$@"
if [ ! -z "$filters" ]
then
    filtered=
    for filter in $filters
    do
        [ ! -z $(echo "$analysers" | grep -P "\b$filter") ] && filtered="$filtered$(echo "$analysers" | grep -P "\b$filter.*?\b") "
    done
    analysers=$filtered
fi
na=$(echo "$analysers" | wc -l)

# Loop through the analysers used in the last pipeline run
[ ! -e $dir_artifact ] && mkdir -p $dir_artifact
ia=1
for analyser in $analysers
do
    echo
    display_info "Analyser $ia of $na: $analyser (Docker Image: ${docker_map[$analyser]})"
    ia=$(($ia +1))
    run_name=$(echo "$analyser" | awk -F'-' '{ print $1 }')

    # Prune any previous artifact
    [ -e "$dir_artifact/$analyser.json.gz" ] && rm -f $dir_artifact/$analyser.json.gz
    # Run the command and capture the output code
    docker run -it --rm --name "sast_${repo}_${run_name}" \
        --volume $dir_root:/builds/debear/sites/$repo \
        --env CI_PROJECT_DIR=/builds/debear/sites/$repo \
        --env SAST_EXCLUDED_PATHS="tests,vendor,node_modules" \
        --env SEARCH_MAX_DEPTH=20 \
        registry.gitlab.com/security-products/${docker_map[$analyser]}
    retcode=$?
    # Move the artifacts around
    [ -e "$dir_root/semgrep.sarif" ] && rm -f $dir_root/semgrep.sarif
    [ -e "$dir_root/gl-sast-report.json" ] && mv $dir_root/gl-sast-report.json $dir_artifact/$analyser.json && gzip $dir_artifact/$analyser.json
    # Abort if we encountered an error
    if [ $retcode -gt 0 ]
    then
        display_error "Failed (Status Code: $err)"
        exit 20
    fi
    # Analyse the report
    analyse_report "$dir_artifact/$analyser.json.gz"
done
summarise_report
