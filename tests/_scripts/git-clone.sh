#!/bin/bash
# Clone a secondary repo in a location we can access

# We must have an argument of a repo name
git_alt=$1
if [ -z "$git_alt" ]
then
    echo "** Git repo argument required, but missing." >&2
    exit 90
fi
# And optionally, a directory to store it (falling back to the repo name if not passed)
[ ! -z "$2" ] && git_alt_dir="$2" || git_alt_dir="$git_alt"

dir_base=$(dirname $0)
dir_root=$(realpath $dir_base/../..)
dir_scripts=$(realpath $dir_base/../_scripts)
dir_repo="$dir_root/../$git_alt_dir"

# Skip if already checked out
if [ -e $dir_repo ]
then
    exit
fi

git_repo=$($dir_scripts/git.sh repo)
git_branch=$($dir_scripts/git.sh branch)
if [ ! -z $CI ]
then
    git_alt_remote="https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/"
else
    git_alt_remote="git@gitlab.com:"
fi
git_alt_remote="${git_alt_remote}debear/sites/$git_alt"
git_alt_vendor="vendor/debear/$git_alt"

cd $dir_root/..
git clone --branch master $git_alt_remote $git_alt_dir
cd $git_alt_dir
# Get $git_alt_dir as an absolute dir
git_alt_dir=$(pwd)
# Switch branch from master to our equivalent branch (as we assume some level of parity)?
if [ "x$git_branch" != 'xmaster' ]
then
    branch_exists=$(git ls-remote --heads origin $git_branch | wc -l)
    if [ $branch_exists -ne 0 ]
    then
        git checkout $git_branch
    fi
fi

# Skeleton-specific additions
if [ "x$git_alt" = 'xskeleton' ]
then
    # Two-steps in case the path is absolute
    tests/_scripts/convert-symlinks.sh
fi

# Setup the repo's vendor folder
cd $dir_root
if [ ! -e $git_alt_vendor/vendor ]
then
    # Setup a base version
    mkdir -p $git_alt_vendor/vendor/debear/setup
    cd $git_alt_vendor/vendor/debear/setup
    # Use the same 'setup' folder as the base repo - linked to absolutely, not relatively
    ln -s $dir_root/vendor/debear/setup
fi
cd $git_alt_dir
[ ! -z $CI_FAUX ] && vendor_ln=$dir_root || vendor_ln=../$git_repo
ln -s $vendor_ln/$git_alt_vendor/vendor

# Composer update required?
cd $git_alt_dir
composer install
