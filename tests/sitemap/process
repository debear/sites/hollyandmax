#!/bin/bash
# Generate and test each URL in the sitemaps to ensure they process cleanly

# Load the common functions
basedir=$(dirname $(realpath $0))
. $basedir/../_scripts/helpers.sh
. $basedir/display.sh
dir_config=$(realpath $basedir/../../config)

# The sites we're processing
domain=$($basedir/../_scripts/git.sh repo)
subsites="$@" # Assume a specific subsites from the command line
if [ -z "$subsites" ]
then
    # None supplied, so get the full list of subsites
    subsites="_ $(find $dir_config -mindepth 2 -maxdepth 2 -type f | sed -r 's/^.+\/([^/]+)\.php$/\1/g')"
else
    # Validate
    for s in $subsites
    do
        if [ "x$s" != 'x_' ] && [ $(ls $dir_config/*/$s.php 2>/dev/null | wc -l) -eq 0 ]
        then
            display_error "Unknown subsite '$s'"
            exit 1
        fi
    done
fi

# Where to store the results
dir_tmp="/tmp/debear/sitemaps-$$"
mkdir -p $dir_tmp

# State we're running
display_title "Testing '$domain' sitemaps"
display_info -n "Run starting:"
echo " $(date)"
display_info -n "Output Dir:"
echo " $dir_tmp"

#
# Generate the sitemaps
#
echo
display_section "Step 1: Generating Sitemaps"
for s in $subsites
do
    # Handle the top-level vs subsite distinction
    if [ "x$s" = 'x_' ]
    then
        sdisp='/'
        sinit='sitemap.xml'
    else
        sdisp=$s
        sinit="$s/sitemap.xml"
    fi
    # Proceed with downloading and parsing the sitemap(s)
    echo -n "$sdisp: "
    bench_sect_start=$(date +'%s.%N')
    sitemaps=("https://$domain.debear.dev/$sinit")
    for ((i = 0; i < "${#sitemaps[@]}"; i++))
    do
        sm=${sitemaps[$i]}
        curl --silent --cookie-jar $dir_tmp/cookies "$sm" --output $dir_tmp/$s.sitemap
        # Additional sitemaps to parse?
        for addtl in $(cat $dir_tmp/$s.sitemap | tr '\n' ' ' | grep -Po '<sitemap>\s*<loc>\K(.*?)(?=</loc>)')
        do
            sitemaps+=($addtl)
        done
        # URLs in this file
        cat $dir_tmp/$s.sitemap | tr '\n' ' ' | grep -Po '<url>\s*<loc>\K(.*?)(?=</loc>)' >>$dir_tmp/$s.urls
    done
    # Benchmark loading and state completion
    num_urls=$(wc -l $dir_tmp/$s.urls | awk '{ print $1 }')
    bench_sect_end=$(date +'%s.%N')
    bench_sect_time=$(echo "scale=3;($bench_sect_end - $bench_sect_start)/1" | bc)
    echo -e "[ Done ] (Found $num_urls URLs in ${#sitemaps[@]} sitemap(s) in ${bench_sect_time}s)"
done

#
# Then test each URL, storing details of each
#
echo
display_section "Step 2: Processing Sitemap URLs"
tot_proc=0
bench_all_start=$(date +'%s.%N')
for s in $subsites
do
    if [ $tot_proc -gt 0 ]; then echo; fi
    if [ "x$s" = 'x_' ]
    then
        display_info "/:"
    else
        display_info "$s:"
    fi
    bench_sect_start=$(date +'%s.%N')

    # Procss the URLs, with a little progress updater
    num_tot=$(wc -l $dir_tmp/$s.urls | awk '{ print $1 }')
    num_proc=0
    for url in $(sed -r 's/(\&)amp;/\1/g' $dir_tmp/$s.urls) # HTML Decode argumet separator
    do
        # Process the URL
        url_status=$(curl "$url" --output /dev/null --silent --head --cookie-jar $dir_tmp/cookies --write-out '%{response_code}|%{url_effective}|%{time_starttransfer}\n'| tee -a $dir_tmp/$s.res)
        # Display the status
        url_status=$(grep -F $url $dir_tmp/$s.res | awk -F'|' '{ print $1 }')
        url_group=${url_status::1}
        if [[ $url_group =~ ^[12]$ ]]
        then
            echo -n '.'
        elif [ $url_group -eq 3 ]
        then
            display_yellow -n $url_group
        elif [[ $url_group =~ ^[45]$ ]]
        then
            display_red -n $url_group
        else
            display_yellow -n '?'
        fi
        # Progress update
        let "num_proc++"; let "tot_proc++"
        if [ $(( $num_proc % 60 )) -eq 0 ]
        then
            # Status, newline
            summarise_progress $num_proc $num_tot
        fi
    done
    summarise_progress $num_proc $num_tot

    # Benchmark calcs and final (section) summary
    bench_sect_end=$(date +'%s.%N')
    bench_sect_time=$(echo "scale=3;($bench_sect_end - $bench_sect_start)/1" | bc)
    echo -e "(Processed $num_proc URLs in ${bench_sect_time}s)"
done
bench_all_end=$(date +'%s.%N')
bench_all_time=$(echo "scale=3;($bench_all_end - $bench_all_start)/1" | bc)
echo -e "(In total, processed $tot_proc URLs in ${bench_all_time}s)\n"

#
# Return the status results
#
unknown=0
warning=0
failed=0
display_section "Step 3: Analysing Results"
# Start grouped by subsite
for s in $subsites
do
    if [ "x$s" = 'x_' ]
    then
        display_info "/:"
    else
        display_info "$s:"
    fi
    # Totals grouped by HTTP status code
    for group in $(sort -n $dir_tmp/$s.res | cut -d'|' -f1 | uniq -c | awk '{ print $2"|"$1 }')
    do
        colourise_status "$group"
    done

    # Summarise the non-2XX URLs
    list_concerns 'unknown' "$(sort -n $dir_tmp/$s.res | grep -v '^[1-5]')"
    list_concerns 'warning' "$(sort -n $dir_tmp/$s.res | grep '^3')"
    list_concerns 'failed' "$(sort -n $dir_tmp/$s.res | grep '^[45]')"
    echo
done
# End with overall figures, grouped by ?XX
display_info "Overall Summary:"
echo "Tot: $tot_proc"
for status in $(sort -n $dir_tmp/*.res | sed -r 's/^([0-9])[0-9]{2}(|.+)/\1/' | cut -d'|' -f1 | uniq)
do
    num_status=$(cat $dir_tmp/*.res | grep -c "^$status")
    colourise_status "${status}XX|$num_status"
done

# State we're finished with an appropriate summary
echo
if [ $failed -eq 1 ]
then
    display_error "Not all tests completed successfully"
    retcode=1
elif [ $warning -eq 1 ]
then
    display_warning "No tests failed, but warning(s) were generated"
    retcode=1
elif [ $unknown -eq 1 ]
then
    display_warning "No tests failed, but unknown status code(s) were generated"
    retcode=1
else
    display_success "All tests completed successfully"
    retcode=0
fi
display_info -n "Run completed:"
echo " $(date)"
exit $retcode
