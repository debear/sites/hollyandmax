#!/bin/bash
# Sitemap processing display helpers

function summarise_progress {
    num=$1; tot=$2
    # Fill in the spaces
    mod=$(( $num % 60 ))
    if [ $mod -gt 0 ]
    then
        missing=$(( 60 - $mod ))
        eval "printf '%.s ' {1..$missing}"
    fi
    # State the progress
    width=$(echo -n $tot | wc -c)
    pct=$(echo "100 * ($num / $tot)" | bc -l)
    printf " %${width}.0d / %${width}.0d (%6.2f%%)\n" $num $tot $pct
}

function list_concerns() {
    state=$1; list=$2
    if [ ! -z "$list" ]
    then
        # Title
        if [ $state = 'failed' ]
        then
            failed=1
            display_red "\nFailure(s):"
        elif [ $state = 'warning' ]
        then
            warning=1
            display_yellow "\nWarnings(s):"
        elif [ $state = 'unknown' ]
        then
            unknown=1
            display_yellow "\nUnknown:"
        fi
        # List
        for url in $list
        do
            colourise_status "$url"
        done
    fi
}

function colourise_status() {
    status=$(echo "${1}" | awk -F'|' '{ print $1 }')
    if [[ $status =~ ^[12] ]]
    then
        # Success
        display_green -n "$status: "
    elif [[ $status =~ ^3 ]]
    then
        # Redirect
        display_yellow -n "$status: "
    elif [[ $status =~ ^[45] ]]
    then
        # Failure
        display_red -n "$status: "
    else
        # Unknown
        display_yellow -n "$status: "
    fi
    # Then the actual count
    echo "${1}" | awk -F'|' '{ print $2 }'
}
