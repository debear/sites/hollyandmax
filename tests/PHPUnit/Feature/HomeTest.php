<?php

namespace Tests\PHPUnit\Feature;

use Tests\PHPUnit\Base\FeatureTestCase;

class HomeTest extends FeatureTestCase
{
    /**
     * A test of the main homepage
     * @return void
     */
    public function testHome(): void
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('<h1>The Holly and Max Gift List</h1>');
        $response->assertSee('<p class="store_code"><strong>Product Code:</strong> 41309002</p>');
    }

    /**
     * A test purchase
     * @return void
     */
    public function testPurchase(): void
    {
        // Make the purchase.
        $response = $this->post('/gift/58/reserve');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/plain; charset=UTF-8');
        $response->assertSee('1::OK');

        // Check the item is no longer there.
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSee('<h1>The Holly and Max Gift List</h1>');
        $response->assertDontSee('<p class="store_code"><strong>Product Code:</strong> 41309002</p>');
    }

    /**
     * A test invalid purchases
     * @return void
     */
    public function testInvalidPurchases(): void
    {
        // Make the original purchase.
        $response = $this->post('/gift/59/reserve');
        $response->assertStatus(200);
        $response->assertHeader('Content-Type', 'text/plain; charset=UTF-8');
        $response->assertSee('1::OK');

        // Then try a second time.
        $response = $this->post('/gift/59/reserve');
        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'text/plain; charset=UTF-8');
        $response->assertSee('0::Invalid Arguments');

        // Something that never existed.
        $response = $this->post('/gift/58923/reserve');
        $response->assertStatus(400);
        $response->assertHeader('Content-Type', 'text/plain; charset=UTF-8');
        $response->assertSee('0::Invalid Arguments');

        // Non-numeric ID.
        $response = $this->post('/gift/ID/reserve');
        $response->assertStatus(405);
    }

    /**
     * A test of the manifest.json file
     * @return void
     */
    public function testManifest(): void
    {
        $response = $this->get('/manifest.json');
        $response->assertStatus(200);
        $response->assertJson([
            'name' => 'Holly &amp; Max',
            'short_name' => 'Holly &amp; Max',
            'description' => 'The Gift List for Holly &amp; Max',
            'start_url' => '/?utm_source=homescreen',
            'icons' => [[
                'src' => 'https://static.debear.test/skel/images/meta/debear.png',
                'sizes' => '512x512',
                'type' => 'image/png',
            ]],
        ]);
    }
}
