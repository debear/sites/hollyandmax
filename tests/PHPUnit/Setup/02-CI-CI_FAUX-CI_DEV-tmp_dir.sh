#!/bin/bash
# During the CI jobs, our storage/compiled paths refer to a (cleaned) location in /tmp
dir_scripts=$(realpath $(dirname $0)/../../_scripts)
git_repo=$($dir_scripts/git.sh repo)

# Prune what may have been there before
rm -rf /tmp/debear/ci/$git_repo

# Create and then link to our new folder
mkdir -p /tmp/debear/ci/$git_repo/{logs,views,merges/js,merges/css,tmp}
