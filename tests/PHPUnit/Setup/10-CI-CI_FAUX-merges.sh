#!/bin/bash
# Convert the merges symlinks into usable directories
dir_root=$(realpath $(dirname $0)/../../..)
rm -rf $dir_root/resources/{css,js}/merged && mkdir -p $dir_root/resources/{css,js}/merged
