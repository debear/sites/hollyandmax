#!/bin/bash
# Split the two stages of a PHPUnit run to make the tests much more efficient

# Load the common functions
basedir=$(dirname $(realpath $0))
. $basedir/../_scripts/helpers.sh
# Re-base to the repo root
cd $basedir/../..

# Where to store our code coverage report
coverage_report="vendor/debear/code-coverage/php.txt"
mkdir -p $(dirname $coverage_report)

# These args should only apply to the initial, 'core', stage
core_args="--coverage-text=${coverage_report} $@"

# Arguments for configuring our code coverage
coverage_args='-dpcov.enabled=1 -dpcov.directory=. -dpcov.exclude="~vendor~"'

# Then run any secondary configs
for x in `ls tests/PHPUnit/*.xml | sort -n`
do
  xn=$(basename $x)
  echo
  display_section "Running against $xn:"
  php $coverage_args vendor/bin/phpunit --configuration $x $core_args

  # Stop on error, returning the same error code
  err=$?
  if [ $err -ne 0 ]
  then
    exit $err
  fi

  # After the first run, we scrap the "core" args
  core_args=
done

# Dump the coverage report
if [ ! -e $coverage_report ]
then
  display_error "Coverage report file unavailable?"
  exit 1
fi
grep -PC3 '^\s*Summary:' $coverage_report

# We expect 100% Code Coverage, so if we don't have that, consider this a fail.
if [ $(grep -Pc '^\s+Lines:\s+100.00%' vendor/debear/code-coverage/php.txt) -eq 0 ]
then
  display_error "Code Coverage is no longer 100%"
  exit 1
fi
