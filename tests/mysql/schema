#!/bin/bash
# Run our migrations on the test database (created earlier)

dir_root=$(realpath $(dirname $0))
dir_base=$(realpath $dir_root/../..)
dir_scripts=$(realpath $dir_root/../_scripts)
mysql_args="-s --host=$DB_HOST --port=$DB_PORT --user=$DB_USERNAME"

. $dir_scripts/helpers.sh

# Validation: as this is for testing, we _MUST_ have a DB_PREFIX set!
if [ -z $DB_PREFIX ]
then
    echo "** Missing required environment variable: DB_PREFIX." >&1
    echo "**  Will not run without it, as it may affect production/development databases." >&1
    exit 9
fi

# Passed a specific repo?
if [ ! -z "$1" ]
then
    git_repo="$1"
# If no specific repo, use the current
else
    git_repo=$($dir_scripts/git.sh repo)
fi

if [ "x$git_repo" = 'xskeleton' ]
then
    is_skel=1
else
    is_skel=0
fi

display_title "Creating database schema for '$git_repo'"

# Determine the directory where we're rooted in
dir_db="$dir_base/../$git_repo/database"
if [ $is_skel -eq 1 ] && [ ! -e "$dir_db" ]
then
    dir_db="$dir_base/../_debear/database"
fi
# Skeleton-specific path
if [ $is_skel -eq 1 ]
then
    dir_db="$dir_db/setup"
fi
cd $dir_db

# Loop through the databases in this repo
total=0
failed=0
for db in $(ls -d *)
do
    echo
    dbname="$DB_PREFIX$db"
    display_section "$db (as '$dbname'):"

    # Sub-grouped schema?
    if [ $(ls $db/schema/*.sql 2>/dev/null | wc -l) -gt 0 ]
    then
        # No, all in one level
        for f in $db/schema/*.sql
        do
            tbl=$(basename "$f")
            display_info "  $tbl"
            # Run the SQL
            mysql $mysql_args $dbname <$f
            # Then process the return code
            if [ $? -ne 0 ]
            then
                failed=$(($failed + 1))
            fi
            total=$(($total + 1))
        done

    else
        # Yes, so find each sub-level and run as its own command
        for sect in $(find $db/* -maxdepth 0 -type d)
        do
            if [ ! -e $sect/schema ]
            then
                # Skip section with no schema
                continue
            fi
            echo
            display_section "$(basename $sect):"
            for f in $sect/schema/*.sql
            do
                tbl=$(basename "$f")
                display_info "  $tbl"
                # Run the SQL
                mysql $mysql_args $dbname <$f
                # Then process the return code
                if [ $? -ne 0 ]
                then
                    failed=$(($failed + 1))
                fi
                total=$(($total + 1))
            done
        done
    fi
done

if [ $failed -gt 0 ]
then
    # Failures
    echo
    display_error "$failed of $total files failed."
    exit 1
fi
