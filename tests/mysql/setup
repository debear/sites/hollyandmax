#!/bin/bash
# Setup the running of MySQL CI tests

dir_base=$(dirname $0)
dir_root=$(realpath $dir_base/../..)
dir_scripts=$(realpath $dir_base/../_scripts)

. $dir_base/helpers
. $dir_scripts/helpers.sh

# Validation: as this is for testing, we _MUST_ have a DB_PREFIX set!
if [ -z $DB_PREFIX ]
then
    echo "** Missing required environment variable: DB_PREFIX." >&1
    echo "**  Will not run without it, as it may affect production/development databases." >&1
    exit 9
fi

# Perform any repo-specific setup
custom_setup 'setup'

# Create our test databases
mysql_args="-s --host=$DB_HOST --port=$DB_PORT --user=$DB_USERNAME"
display_title "Creating test database(s):"
for db_arg in $(env | awk -F "=" '{print $1}' | grep -P 'DB_.*?DATABASE')
do
    db=${!db_arg} # Gets the value in the variable
    display_section "  $db"
    echo "DROP DATABASE IF EXISTS $db; CREATE DATABASE $db;" | mysql $mysql_args
done
display_info "    - Complete!"
