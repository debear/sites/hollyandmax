## Build v3.0.187 - d82a159 - 2025-02-22 - SoftDep: February 2025
* d82a159: Update PHP.ini locations to the new docker image location (_Thierry Draper_)
* 4c4dd9d: Migrate config getting to the framework Config class (_Thierry Draper_)

## Build v3.0.185 - 925ab05 - 2025-01-17 - SoftDep: January 2025
* (Auto-commits only)

## Build v3.0.185 - 9aff21f - 2024-12-15 - SoftDep: December 2024
* (Auto-commits only)

## Build v3.0.185 - 14e753f - 2024-11-16 - SoftDep: November 2024
* (Auto-commits only)

## Build v3.0.185 - 69323b0 - 2024-10-21 - SoftDep: October 2024
* (Auto-commits only)

## Build v3.0.185 - f274537 - 2024-09-15 - SoftDep: September 2024
* (Auto-commits only)

## Build v3.0.185 - bba50e1 - 2024-09-15 - SysAdmin: CI Tweaks
* bba50e1: Catch Phan run errors that could cause cyclical self-calling (_Thierry Draper_)
* 073fb78: Fix first run setup for downloading seed data (_Thierry Draper_)
* 4360b66: Apply some minor CI script tidying and improvements (_Thierry Draper_)

## Build v3.0.182 - 2d835d3 - 2024-08-19 - SoftDep: August 2024
* (Auto-commits only)

## Build v3.0.182 - 91b7b00 - 2024-08-09 - SysAdmin: MariaDB to MySQL Migration
* 91b7b00: Migrate our CI from MariaDB to MySQL (_Thierry Draper_)

## Build v3.0.181 - ffa7d79 - 2024-07-17 - Hotfix: SAST Reporting
* ffa7d79: Fix the SAST report download sort ordering (_Thierry Draper_)

## Build v3.0.180 - b6a527d - 2024-07-17 - Feature: CI Server Sync Job
* b6a527d: Include the server sync database to our CI job list (_Thierry Draper_)

## Build v3.0.179 - e984153 - 2024-07-16 - SoftDep: July 2024
* (Auto-commits only)

## Build v3.0.179 - f448bdd - 2024-06-21 - Hotfix: Local CI Job Parsing
* f448bdd: Fix the local CI job parser to exclude comments after the allow_failure flag (_Thierry Draper_)

## Build v3.0.178 - 41aca01 - 2024-06-18 - SoftDep: June 2024
* (Auto-commits only)

## Build v3.0.178 - 66bc882 - 2024-06-17 - Hotfix: CI Downloads
* 66bc882: Fix the request sent to determine what CI data files need downloading (_Thierry Draper_)
* 53b6b9e: Improve the location of the CI download trust store cert (_Thierry Draper_)

## Build v3.0.176 - ef0fea0 - 2024-05-18 - SoftDep: May 2024
* (Auto-commits only)

## Build v3.0.176 - f1fa773 - 2024-05-13 - Security: CI Jobs
* f1fa773: Switch our localci SAST exclusion to the new emulator (_Thierry Draper_)
* 521256f: Implement a local SAST emulator script using the GitLab docker images (_Thierry Draper_)
* df47316: Implement an initial SAST reporting tool (_Thierry Draper_)
* e451825: Migrate our only/except job logic to the preferred rules format (_Thierry Draper_)
* 2a81ee9: Incorporate the GitLab managed SAST pipeline job (_Thierry Draper_)
* 658472a: Add some native tool dependency vulnerability scanning within our CI (_Thierry Draper_)

## Build v3.0.170 - 055d7c7 - 2024-04-27 - Improvement: CI Downloads
* 055d7c7: Switch CI downloads from wget to curl (_Thierry Draper_)

## Build v3.0.169 - 1df78c3 - 2024-04-24 - Improvement: JavaScript Formatting
* 1df78c3: Disable the eslint unused disabled rule checked whilst linting, as it conflicts with the standards check (_Thierry Draper_)
* 53f9fda: Migrate to eslints flat config format (_Thierry Draper_)

## Build v3.0.167 - 38b8964 - 2024-04-21 - SoftDep: April 2024
* (Auto-commits only)

## Build v3.0.167 - f658b75 - 2024-04-18 - Improvement: CI ECMAScript Version
* f658b75: Update the targeted version of ECMAScript in CI to v13 / 2022 (_Thierry Draper_)

## Build v3.0.166 - 10769d9 - 2024-03-17 - SoftDep: Laravel 11 and March 2024 Update
* 10769d9: Upgrade dependencies to PHPUnit 11 (_Thierry Draper_)
* c6409ad: Switch use of the now-removed $dates property in Eloquent to its $casts equivalent (_Thierry Draper_)
* 6f31663: Upgrade dependencies to PHPUnit 10 (_Thierry Draper_)

## Build v3.0.163 - 2d3bd1b - 2024-02-17 - SoftDep: February 2024
* (Auto-commits only)

## Build v3.0.163 - 5ba117d - 2024-01-27 - Improvement: Refactoring JavaScript
* 5ba117d: Revert PHPCS "linting" enforced JavaScript standards for curly brace positioning (_Thierry Draper_)
* 31f4aa6: Replace the JavaScript linter with something less enforcing of (clunky) standards (_Thierry Draper_)
* 5249a0f: Move to greater use of the arrow functions (_Thierry Draper_)
* 9ebcd03: Replace use of the deprecated DOM.get* methods in favour of the generalised query selector (_Thierry Draper_)

## Build v3.0.159 - 3efd237 - 2024-01-19 - Improvement: JavaScript Null Operators
* 3efd237: Make use of the optional chaining JS operator to simplify the code (_Thierry Draper_)
* 1f5d177: Update eslints base ECMAScript version to allow for null-handling simplification (_Thierry Draper_)

## Build v3.0.157 - 7f19560 - 2024-01-14 - SoftDep: January 2024
* (Auto-commits only)

## Build v3.0.157 - 0e4c4de - 2023-12-17 - SoftDep: December 2023
* (Auto-commits only)

## Build v3.0.157 - 8b22f9a - 2023-11-27 - CI: Standardised CSS Standards
* 8b22f9a: Apply our standard 120-char width for CSS standards rather than the default 80 (_Thierry Draper_)

## Build v3.0.156 - 857ae38 - 2023-10-14 - Composer: October 2023
* 857ae38: Backport a CI fix from earlier skeleton CDN update (_Thierry Draper_)

## Build v3.0.155 - e1945ac - 2023-09-18 - Composer: September 2023
* (Auto-commits only)

## Build v3.0.155 - f284222 - 2023-08-30 - Improvement: CI SymLink Conversion
* f284222: Switch to the new symlink-to-dir CI converter (_Thierry Draper_)

## Build v3.0.154 - 42a9d28 - 2023-08-05 - Composer: August 2023
* (Auto-commits only)

## Build v3.0.154 - 094ff02 - 2023-07-15 - Composer: July 2023
* (Auto-commits only)

## Build v3.0.154 - 7ec4507 - 2023-06-17 - Composer: June 2023
* (Auto-commits only)

## Build v3.0.154 - 304588e - 2023-05-14 - Composer: May 2023
* (Auto-commits only)

## Build v3.0.154 - 87d9e7b - 2023-05-02 - SysAdmin: Deprecations
* 87d9e7b: Replace use of fgrep with grep given it is being flagged overtly as obsolete (_Thierry Draper_)

## Build v3.0.153 - 078b0ba - 2023-04-15 - Composer: April 2023
* (Auto-commits only)

## Build v3.0.153 - deec5bd - 2023-03-11 - Composer: March 2023
* (Auto-commits only)

## Build v3.0.153 - e8d9a75 - 2023-03-01 - Improvement: CSS Standards to Prettier
* e8d9a75: Fix CSS standards according to the new Prettier rules (_Thierry Draper_)
* da6608e: Switch the deprecated Stylelint CSS standards to Prettier (_Thierry Draper_)

## Build v3.0.151 - 67148a3 - 2023-02-12 - Composer: February 2023
* (Auto-commits only)

## Build v3.0.151 - be00dc4 - 2023-01-15 - Composer: January 2023
* (Auto-commits only)

## Build v3.0.151 - ec683f2 - 2023-01-08 - SysAdmin: CI Docker Images from Container Registry
* ec683f2: Switch the CI jobs to images from our Container Registry (_Thierry Draper_)

## Build v3.0.150 - be6efe7 - 2023-01-05 - Improvement: MySQL Linting
* be6efe7: Switch the CI MariaDB collation to latin1_general (_Thierry Draper_)
* 449ac39: Include the CI job for linting setup scripts (_Thierry Draper_)

## Build v3.0.148 - b113a1a - 2022-12-18 - Composer: December 2022
* (Auto-commits only)

## Build v3.0.148 - 2721a9d - 2022-11-19 - Composer: November 2022
* (Auto-commits only)

## Build v3.0.148 - f18093b - 2022-10-16 - Composer: October 2022
* (Auto-commits only)

## Build v3.0.148 - 6868856 - 2022-09-18 - Composer: September 2022
* (Auto-commits only)

## Build v3.0.148 - db24fcf - 2022-09-15 - CI: Environment dependency fix
* db24fcf: Install wget as a CI environment dependency (_Thierry Draper_)

## Build v3.0.147 - 721eb66 - 2022-08-14 - Composer: August 2022
* (Auto-commits only)

## Build v3.0.147 - bbe96d1 - 2022-07-19 - SysAdmin: Backport for PHP 8.0
* bbe96d1: Update the composer file to allow PHP 8.0, as well as the previous 8.1 (_Thierry Draper_)

## Build v3.0.146 - 96e79dc - 2022-07-16 - Composer: July 2022
* (Auto-commits only)

## Build v3.0.146 - 0cc367f - 2022-07-15 - Hotfix: PHPMD TooManyMethods whitelist
* 0cc367f: Increase the scope of PHPMDs TooManyMethods whitelist (_Thierry Draper_)

## Build v3.0.145 - 63e9e4d - 2022-06-19 - Composer: June 2022
* (Auto-commits only)

## Build v3.0.145 - cb3333f - 2022-06-08 - SysAdmin: Revert to native Blade data displaying
* cb3333f: Re-factor our Blade template variable echoing to use the appropriate native Laravel mechanism (_Thierry Draper_)
* 51ba8df: Move common DeBear helper classes to the alias list for use in Blade templates (_Thierry Draper_)
* 29c7af4: Revert use of @print and @config in Blade templates with the native Laravel method (_Thierry Draper_)
* 346ce22: Restore Blade linting using the new dependency (_Thierry Draper_)

## Build v3.0.141 - a578854 - 2022-05-27 - CI: PHP Code Coverage streamlining
* a578854: Streamline the PHP Code Coverage output, and consider output less than 100% as a failure (_Thierry Draper_)

## Build v3.0.140 - 95de4f2 - 2022-05-26 - CI: Report PHP Unit code coverage on master
* 95de4f2: Propagate the PHP Unit code coverage report to the deploy stage for its result to be tagged against the master branch (_Thierry Draper_)

## Build v3.0.139 - 5000581 - 2022-05-22 - CI: Switch Code Coverage Driver
* 5000581: Switch Code Coverage from using XDebug to PCOV (_Thierry Draper_)

## Build v3.0.138 - 5a40303 - 2022-05-22 - Improvement: Unit Tests as part of Code Coverage
* 5a40303: Add the GitLab code coverage regexp pattern, previously configured within the UI (_Thierry Draper_)
* c3c1cf8: Refine our Unit Tests according to localci tweaks made to the skeleton (_Thierry Draper_)
* 66e62c1: Include appropriate PHPUnit code in our code coverage tests (_Thierry Draper_)

## Build v3.0.135 - 3e04005 - 2022-05-22 - Improvement: Laravel 9
* 3e04005: Remove Blade linting that is no longer available following the Laravel 9 upgrade (_Thierry Draper_)

## Build v3.0.134 - 6dbb17b - 2022-05-21 - SysAdmin: PHP 8
* b3af5f7: Use a new stylelint rule modifier to prevent recent false negatives (_Thierry Draper_)
* d6cb7ee: Add a new CI step to lint Blade templates (_Thierry Draper_)
* 930fe7a: Include a repo-specific version of the Phan PHP Standards CI job (_Thierry Draper_)
* 1a0eddf: Switch from Laravel's on-the-fly class facades at the root level to their actual full path (_Thierry Draper_)

## Build v3.0.130 - ada2f3d - 2021-11-21 - Hotfix: Confirmation Overlap Position
* ada2f3d: Correct the confirmation z-index for both another gift idea AND the page footer (_Thierry Draper_)

## Build v3.0.129 - c860b01 - 2021-11-13 - Composer: November 2021
* (Auto-commits only)

## Build v3.0.129 - e333853 - 2021-11-12 - CI: CSS Property Standards
* e333853: Update CSS property rules to reflect the new alphabetical standards (_Thierry Draper_)
* 7f56b10: Add new stylelint standards test for alphabetical CSS property ordering (_Thierry Draper_)
* 5a984fe: Remove a recently deprecated stylelint rule (_Thierry Draper_)

## Build v3.0.126 - b200449 - 2021-10-16 - Composer: October 2021
* (Auto-commits only)

## Build v3.0.126 - 0d1d8d6 - 2021-09-18 - Composer: September 2021
* (Auto-commits only)

## Build v3.0.126 - a4a8232 - 2021-08-13 - Composer: August 2021
* (Auto-commits only)

## Build v3.0.126 - 99aebcf - 2021-08-13 - Hotfix: PHPUnit XML .env spec
* 99aebcf: Add missing .env values to the PHPUnit test XML spec (_Thierry Draper_)

## Build v3.0.125 - d89b1ee - 2021-07-27 - Feature: Sitemap URL tester
* d89b1ee: Sitemap parser and processor for URL testing (_Thierry Draper_)

## Build v3.0.124 - 5873d52 - 2021-07-17 - Composer: July 2021
* (Auto-commits only)

## Build v3.0.124 - 80ec53e - 2021-06-21 - Composer: June 2021
* (Auto-commits only)

## Build v3.0.124 - 90154e2 - 2021-05-15 - Composer: May 2021
* (Auto-commits only)

## Build v3.0.124 - 5a7c555 - 2021-04-30 - CI: Pipeline Tweaks
* 5a7c555: Make use of the CI's needs: option to streamline job and stage links (_Thierry Draper_)

## Build v3.0.123 - 0616228 - 2021-04-17 - Composer: April 2021
* (Auto-commits only)

## Build v3.0.123 - 7fc3549 - 2021-03-13 - Composer: March 2021
* (Auto-commits only)

## Build v3.0.123 - 2e6cac3 - 2021-02-13 - Composer: February 2021
* (Auto-commits only)

## Build v3.0.123 - 892544a - 2021-01-16 - Composer: January 2021
* (Auto-commits only)

## Build v3.0.123 - f0af2a6 - 2021-01-11 - Improvement: Password Policy Skeleton Changes
* f0af2a6: Rely on an appropriate version of guzzlehttp/guzzle after adding the password policy (_Thierry Draper_)

## Build v3.0.122 - c0db9aa - 2021-01-03 - CI: Only deploy on master branch
* c0db9aa: Only deploy during CI/AD of the master branch (_Thierry Draper_)

## Build v3.0.121 - d179dcf - 2020-12-20 - Composer: December 2020
* (Auto-commits only)

## Build v3.0.121 - dd73326 - 2020-12-05 - Hotfix: Bottom Row Hidden Confirm
* dd73326: Fixes for the switch from Xdebug 2 to 3 (_Thierry Draper_)
* e7a84a4: Fix z-index's for the hidden confirmation line (_Thierry Draper_)

## Build v3.0.119 - c191b6b - 2020-11-14 - Composer: November 2020
* (Auto-commits only)

## Build v3.0.119 - aeebc28 - 2020-10-16 - Improvement: Unit Test manifest.json
* aeebc28: Add the manifest.json file to the suite of Unit Tests (_Thierry Draper_)

## Build v3.0.118 - 7e61e79 - 2020-10-09 - Composer: Upgrade to Laravel 8
* 7e61e79: Update our CI rules following the Laravel 8 (+deps) upgrade (_Thierry Draper_)
* b00816e: Update dependencies from upgrading from Laravel 7 to 8 (_Thierry Draper_)

## Build v3.0.116 - a04e15c - 2020-09-26 - Improvement: Migrations to Schema
* a04e15c: Switch from Migrations to Schemas updated via schema-sync (_Thierry Draper_)

## Build v3.0.115 - 82ed2ae - 2020-09-19 - Composer: September 2020
* (Auto-commits only)

## Build v3.0.115 - c43c9da - 2020-09-19 - CI: Merge PHP Standards Jobs
* c43c9da: Merge the PHP Mess Detector and Standards tests into a single job (_Thierry Draper_)

## Build v3.0.114 - b5c499d - 2020-08-15 - Composer: August 2020
* (Auto-commits only)

## Build v3.0.114 - ecf6c0e - 2020-07-13 - Composer: July 2020
* (Auto-commits only)

## Build v3.0.114 - 17be7c3 - 2020-06-17 - Hotfix: PHPUnit Test Locale
* 17be7c3: Fix PHPUnit tests on CI as the locale does not match dev/prod environments (_Thierry Draper_)

## Build v3.0.113 - cb1cfaf - 2020-06-14 - Composer: June 2020
* (Auto-commits only)

## Build v3.0.113 - b10feda - 2020-06-13 - SysAdmin: Reduce Release Overlap
* b10feda: Reduce the overlap time between releases when performing the release (_Thierry Draper_)

## Build v3.0.112 - 82caa62 - 2020-06-06 - Improvement: PSR-4-like Standards Check
* 82caa62: Add PSR-4 like standards checks, given recent class and file name mis-matches (_Thierry Draper_)

## Build v3.0.111 - be9066d - 2020-06-02 - CI: Local PHPUnit Dev Setup
* be9066d: Add scripts to setup a local dev environment for working on PHPUnit tests (_Thierry Draper_)

## Build v3.0.110 - 88f8441 - 2020-05-18 - Composer: May 2020
* (Auto-commits only)

## Build v3.0.110 - 8ea132a - 2020-04-21 - Composer: April 2020
* (Auto-commits only)

## Build v3.0.110 - ca359ea - 2020-04-07 - Improvement: Dynamic PHPUnit Setup Components
* ca359ea: Standarise and split the PHPUnit setup script into dynamic components (_Thierry Draper_)

## Build v3.0.109 - 131e303 - 2020-04-06 - Hotfix: PHPUnit Error Handling
* 131e303: Ensure the new PHPUnit run script errors on failure at the appropriate stage (_Thierry Draper_)

## Build v3.0.108 - 4641fec - 2020-04-04 - Composer: Replacing PHP Linter
* 4641fec: Replace a deprecated PHP Linting package with its replacement (_Thierry Draper_)

## Build v3.0.107 - b978b7f - 2020-04-04 - Improvement: PHPUnit Efficiency
* b978b7f: Split the PHPUnit run in to a more efficient process (_Thierry Draper_)

## Build v3.0.106 - 7048ced - 2020-03-14 - Composer: March 2020
* (Auto-commits only)

## Build v3.0.106 - 324ce5a - 2020-02-14 - Composer: February 2020
* (Auto-commits only)

## Build v3.0.106 - 97abefb - 2020-01-31 - SysAdmin: .gitignore for env config
* 97abefb: Updated gitignore to hide a local env config file (_Thierry Draper_)

## Build v3.0.105 - d38678e - 2020-01-12 - Composer: January 2020
* (Auto-commits only)

## Build v3.0.105 - 0bf6aa0 - 2019-12-31 - SysAdmin: Prune SCHEMA server-sync steps
* 0bf6aa0: Prune the code/schema server-sync steps we've new mechanisms for (_Thierry Draper_)

## Build v3.0.104 - 94c220f - 2019-12-26 - Hotfix: PHPUnit CI Job Shell Errors
* 94c220f: Fix tput/clear errors during the PHPUnit CI job (_Thierry Draper_)

## Build v3.0.103 - 39b8e97 - 2019-12-15 - Composer: December 2019
* (Auto-commits only)

## Build v3.0.103 - 3196c5e - 2019-11-19 - CI: PHP Comments Standards
* 3196c5e: Fall-out from the new PHP comments standards being enforced (_Thierry Draper_)
* 430e48d: Extend our PSR-12 standards check to include the format of comments (_Thierry Draper_)

## Build v3.0.101 - aec5f88 - 2019-11-16 - Composer: November 2019
* (Auto-commits only)

## Build v3.0.101 - 2d1ff7b - 2019-11-14 - CI: YAML Fixes and Improvements
* 2d1ff7b: CI YAML fixes and improvements, such as moving the PHP/MySQL versions used to GitLab (_Thierry Draper_)

## Build v3.0.100 - f63d919 - 2019-11-05 - Composer: Laravel 6 Fallout
* f63d919: Switch from using the Input:: facade to Request::, as it was removed in Laravel 6 (_Thierry Draper_)

## Build v3.0.99 - 555e012 - 2019-10-23 - Composer: October 2019
* 555e012: PSR-12 whitespace fixes (_Thierry Draper_)

## Build v3.0.98 - 978d946 - 2019-10-21 - Improvement: CSS Linting
* 978d946: CSS tweaks from new linting and standards checks (_Thierry Draper_)
* f46b7a2: Switch the CSS Linting test to use stylelint, and split in to both linting and standards (_Thierry Draper_)

## Build v3.0.96 - 3a207c9 - 2019-09-25 - Composer: September 2019
* (Auto-commits only)

## Build v3.0.96 - 2f07da9 - 2019-09-02 - CI: Fix 'Clean' Local Runs
* 2f07da9: Fix the 'clean' running of our CI locally (_Thierry Draper_)

## Build v3.0.95 - b52a6fc - 2019-08-27 - Composer: August 2019
* (Auto-commits only)

## Build v3.0.95 - 24525e1 - 2019-08-09 - CD: Move Server Details to Env Vars
* 24525e1: Move the deploy server access details to environment variables (_Thierry Draper_)

## Build v3.0.94 - e183f5b - 2019-07-21 - Composer: July 2019
* (Auto-commits only)

## Build v3.0.94 - b37d726 - 2019-07-17 - Feature: Monitor PHP Code Coverage
* b37d726: Enable PHP code coverage report during CI (_Thierry Draper_)

## Build v3.0.93 - b972757 - 2019-07-10 - Hotfix: Skip Deploy in LocalCI
* b972757: The new deploy stage should not be included in our localci run script (which also requires the script: rule to be last) (_Thierry Draper_)

## Build v3.0.92 - e77ed2e - 2019-07-05 - Feature: Enable Continuous Delivery
* e77ed2e: Envoy script for implementing our Continuous Delivery (_Thierry Draper_)

## Build v3.0.91 - 1c90134 - 2019-06-18 - CI: PHPUnit vendor simplification
* 1c90134: Fix a unit test issue because of gift_id re-use? (_Thierry Draper_)
* 9cc47c5: May need to create the base CPAN CI dir (_Thierry Draper_)
* c1e1d4c: Fix, and where appropriate merge, vendor folder creation within the CI (_Thierry Draper_)
* 776bf17: Installing the skeleton for CI is a simpler process (_Thierry Draper_)
* df87465: Ensure composer has a vendor folder to be installed into (_Thierry Draper_)
* 45ad677: Propagate the CI_FAUX flag when running CI tests locally, but from clean (_Thierry Draper_)

## Build v3.0.85 - 9ea3801 - 2019-06-09 - SysAdmin: Sprites to CDN
* 9ea3801: Move the sprites to the CDN (_Thierry Draper_)

## Build v3.0.84 - ae51b48 - 2019-06-08 - CI: Handle Perl Upgrades
* ae51b48: Handle changing versions of perl in the CI (_Thierry Draper_)

## Build v3.0.83 - bf6da70 - 2019-06-07 - CI: Data Download Errors
* bf6da70: When downloading test CI data return an error if the download fails, rather than assume it was a 204/skip (_Thierry Draper_)

## Build v3.0.82 - b498ce5 - 2019-05-21 - CI: Fix PHPUnit skeleton setup
* b498ce5: rsync management of PHPUnit test setup fix (_Thierry Draper_)

## Build v3.0.81 - 2192812 - 2019-05-02 - Migrations: Merge w/Stored Procedures
* 2192812: Re-organise the database migrations layouts to keep all sub-grouped migrations/stored procs together (_Thierry Draper_)

## Build v3.0.80 - 73c124a - 2019-04-24 - Migrations: Connection-appropriate DROP TABLES
* 73c124a: Migration "drop tables" needs appropriate Laravel connection applied (_Thierry Draper_)

## Build v3.0.79 - 84212f2 - 2019-04-22 - CI: Testing Migrations
* 84212f2: Include running the database migrations properly as part of the MySQL Linting CI job (plus testing their rollbacks) (_Thierry Draper_)

## Build v3.0.78 - b10f5ac - 2019-04-16 - Composer: April 2019 updates
* b10f5ac: Some local 'clean' CI test fixes and code optimisations (_Thierry Draper_)

## Build v3.0.77 - 5d096b7 - 2019-04-09 - CI: Fix Logging
* 5d096b7: Automatically fix the logs when cloning the skeleton within CI (_Thierry Draper_)

## Build v3.0.76 - 52d2b91 - 2019-04-08 - CI: Check Migrations
* 52d2b91: Add the migrations to our CI, including a PSR-12 workaround for Laravel's migrations (who do not follow PSR-4's fully qualified class name) (_Thierry Draper_)
* 44f45cd: Re-jig how we run Laravel's migrations script should we sub-filter our migrations (_Thierry Draper_)

## Build v3.0.74 - 7b6b9ee - 2019-03-29 - CI: Exclude Tags
* 7b6b9ee: Prevent CI when pushing tags (_Thierry Draper_)

## Build v3.0.73 - e167e2d - 2019-03-26 - CI: Re-grouped Git remote
* e167e2d: Reflect re-grouping of the git remotes in our CI scripts (_Thierry Draper_)

## Build v3.0.72 - af63e91 - 2019-03-22 - CI: MySQL Linter
* af63e91: Minor changes as part of importing a MySQL linter (_Thierry Draper_)

## Build v3.0.71 - e27699d - 2019-03-12 - Hotfix: Post Launch
* 5a52ec1: Start using a singular composer cache to try and cut down on CI pipeline build time (_Thierry Draper_)
* 8d937e7: Revise the way test databases are created, and include some post-test cleanup of them (_Thierry Draper_)
* 583e20c: CI refinements to the Composer vendor caching rules and include a new Perl-linting process (_Thierry Draper_)
* 9535c26: Make the unit tests compatible with some skeleton filesystem tweaks (_Thierry Draper_)
* 4c5b040: Test script tweaks to attempt repo-name parity between repo and skeleton (_Thierry Draper_)
* 535b601: Root-level merged folder isn't actually required (_Thierry Draper_)
* 3d58a88: Add symlinks required from live (_Thierry Draper_)

## Build v3.0.64 - 5d9235e - 2019-02-17 - Launching Laravel - [![Version: 3.0](https://img.shields.io/badge/Version-3.0-brightgreen.svg)](https://gitlab.com/debear/hollyandmax/tree/v3.0)
* 5d9235e: Git tidy pre merge of Laravel branch (_Thierry Draper_)
* 8c74688: Make the VERSION info part of the repo (_Thierry Draper_)
* 514432b: Minor CI tidying (_Thierry Draper_)
* f8bc8f1: Migration fix, plus some minor tidying up that came from debugging it (_Thierry Draper_)
* 301037b: Prune old MySQL seed data, plus build schema from migrations need downloaded seed (_Thierry Draper_)
* b151987: Migrations for HollyAndMax (_Thierry Draper_)
* 055f142: Regular Composer update (_Thierry Draper_)
* bdccb06: Implement "feature" PHPUnit tests (_Thierry Draper_)
* 3009f61: Fix the gift purchase method that was missing a dependency (_Thierry Draper_)
* eda40da: CI composer fix to only install missing/out-of-date packages rather than update outside of composer.lock context (_Thierry Draper_)
* 4a6fefc: Refine the way sub-site routes are loaded (_Thierry Draper_)
* efd952d: Fallout from the fixes from the new JS standards test (_Thierry Draper_)
* 5852a14: Fixes from the new JS standards test (_Thierry Draper_)
* 552849a: Include extended JS standards checking in the CI (_Thierry Draper_)
* 19215d2: Apply TitleCase notation to JavaScript classes and camelCase notation to JavaScript methods (_Thierry Draper_)
* 54df3a4: Script to run the CI tests locally (and accessing the composer libs directly, rather than through a git-runner) (_Thierry Draper_)
* 74a82d9: Add PSR and Pipeline Status flags to the README (_Thierry Draper_)
* 8f8f0b4: Fallout from applying PSR-12 to the skeleton (_Thierry Draper_)
* 88b46f2: Update the CONTRIBUTING.md file following confirmation of PSR use (_Thierry Draper_)
* fc6b34b: Remove Blade templates from the PSR checks, as they aren't suitable/relevant (_Thierry Draper_)
* 50bde21: Add PSR-12 compliance testing to our CI (_Thierry Draper_)
* 3ef2813: Remove inline control structures, as well as some of the PSR errors flagged by phpcs (_Thierry Draper_)
* f1181cb: JS linting fixes using phpcbf (_Thierry Draper_)
* 4fe311c: Add some CSS and JS linting to our CI (_Thierry Draper_)
* eb83816: PHPMD definition tweaks (_Thierry Draper_)
* ff1d4f8: Switch PHP we are testing against from 7.3 to 7.2 (_Thierry Draper_)
* 90711f7: Initial project CI (_Thierry Draper_)
* 82c6992: Latest version of the icon sprite from recent skeleton changes (_Thierry Draper_)
* 3339eeb: Minor LICENSE file tidying (_Thierry Draper_)
* 356acbd: Streamline the icon sprite (_Thierry Draper_)
* f1258ec: Tweaked iconset CSS naming (_Thierry Draper_)
* ddd6557: Add the page's meta description (_Thierry Draper_)
* 5948f20: Updated iconset for the nav changes (_Thierry Draper_)
* 40cb174: Move the config away from "GA" specifically to a more generic term (_Thierry Draper_)
* dcdff6e: Standardise the Sitemap and Nav links to the new model (_Thierry Draper_)
* c762107: Remove all the header links at the top of the page (_Thierry Draper_)
* 1c9ad96: Model tweaks to use our new customised base implementation (_Thierry Draper_)
* c055888: Minor switching from PHPs $_SERVER to Laravel's Input::server (_Thierry Draper_)
* f1a99a7: Re-worked footer link disabling (_Thierry Draper_)
* bf6a582: Laravel version of the Gift List (_Thierry Draper_)
* 3c583c9: Ignore Home/Sitemap footer links (as single page site) (_Thierry Draper_)
* af6a42a: Initial layout tweaks for the new engine (_Thierry Draper_)
* f5b14b7: Base resources and config from www setup work (_Thierry Draper_)
* 2a73127: Proof-of-concept route (_Thierry Draper_)
* 01da182: Start of process of switching from Homebrew skeleton to Laravel (_Thierry Draper_)
* 5d1bcb3: Removal of the 'hosted dev' environment on production (_Thierry Draper_)

## Build v2.0.16 - 98d2c53 - 2018-06-10 - Homebrew
* _Changelog truncated for initial build_
