<?php

namespace DeBear\Models\HollyAndMax;

use DeBear\Implementations\Model;

class Purchases extends Model
{
    /**
     * MySQL Connection string this Model belongs in
     * @var string
     */
    protected $connection = 'mysql_www';
    /**
     * Database table name this Model associates to
     * @var string
     */
    protected $table = 'HOLLYANDMAX_PURCHASES';
    /**
     * Primary Key column for the database table
     * @var string
     */
    protected $primaryKey = 'purchase_id';
    /**
     * Date columns within the database table
     * @var array
     */
    protected $casts = [
        'when_done' => 'datetime',
    ];
    /**
     * No standard timestamp columns
     * @var boolean
     */
    public $timestamps = false;
}
