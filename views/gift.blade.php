<li class="gift cat-{!! Strings::codify($gift->cat_name) !!} store-{!! Strings::codify($gift->store_name) !!} grid-1-3 grid-tp-1-2 grid-m-1-1" data-id="{!! $gift->gift_id !!}">
    <article data-id="{!! $gift->gift_id !!}">
        <h3>{!! $gift->name !!}</h3>
        <a class="img" href="{!! $gift->info_url !!}" rel="noopener noreferrer" target="_blank">
            <img src="{!! $gift->image !!}" alt="{!! $gift->name !!}">
        </a>
        <p class="price"><strong>Price:</strong> &pound;{!! $gift->price !!}</p>
        <p class="store">
            <strong>Available From:</strong>
            <a @if ($gift->store_logo)class="img"@endif href="{!! $gift->info_url !!}" rel="noopener noreferrer" target="_blank">
                @if ($gift->store_logo)
                    <img src="{!! $gift->store_logo !!}" alt="{!! $gift->store_name !!}">
                @else
                    {!! $gift->store_name !!}
                @endif
            </a>
        </p>
        <p class="store_code"><strong>Product Code:</strong> {!! $gift->product_code !!}</p>
        <p class="reserve">
            <a>I have bought this gift for you</a>
        </p>
        <p class="reserve_details hidden">
            <button class="btn_small btn_green">Confirm</button>
        </p>
        <p class="message hidden">
            <span class="loading">Please Wait, Saving...</span>
        </p>
    </article>
</li>
