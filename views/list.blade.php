@extends('skeleton.layouts.html')

@section('content')

<h1>The Holly and Max Gift List</h1>

<p class="intro">Please don&#39;t feel you need to stick to the present ideas in the list below but equally if you do purchase something from it, please mark it accordingly so it disappears from the list to others.</p>

<ul class="inline_list filter what">
    <li>
        <label for="filter_what_all">View All</label>
        <input type="radio" name="filter_what" id="filter_what_all" value="all" checked="checked"><span class="icon_valid"></span>
    </li>
    <li>
        <label for="filter_what_cat">View by Category</label>
        <input type="radio" name="filter_what" id="filter_what_cat" value="cat"><span class="icon_valid"></span>
    </li>
    <li>
        <label for="filter_what_store">View by Store</label>
        <input type="radio" name="filter_what" id="filter_what_store" value="store"><span class="icon_valid"></span>
    </li>
</ul>

<ul class="inline_list filter cat hidden">
    <li>
        <label for="filter_cat_all">Show All</label>
        <input type="radio" name="filter_cat" id="filter_cat_all" value="all" checked="checked"><span class="icon_valid"></span>
    </li>
    @foreach ( $cats as $cat )
        @php
            $code = Strings::codify($cat);
        @endphp
        <li>
            <label for="filter_cat_{!! $code !!}">{!! $cat !!}</label>
            <input type="radio" name="filter_cat" id="filter_cat_{!! $code !!}" value="{!! $code !!}"><span class="icon_valid"></span>
        </li>
    @endforeach
</ul>

<ul class="inline_list filter store hidden">
    <li>
        <label for="filter_store_all">Show All</label>
        <input type="radio" name="filter_store" id="filter_store_all" value="all" checked="checked"><span class="icon_valid"></span>
    </li>
    @foreach ( $stores as $store )
        @php
            $code = Strings::codify($store);
        @endphp
        <li>
            <label for="filter_store_{!! $code !!}">{!! $store !!}</label>
            <input type="radio" name="filter_store" id="filter_store_{!! $code !!}" value="{!! $code !!}"><span class="icon_valid"></span>
        </li>
    @endforeach
</ul>

<ul class="inline_list gifts clearfix">
    @each('hollyandmax.gift', $gifts, 'gift')
</ul>

@endsection
