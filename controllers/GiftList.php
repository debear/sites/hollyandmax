<?php

namespace DeBear\Http\Controllers\HollyAndMax;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config as FrameworkConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use DeBear\Http\Controllers\Controller;
use DeBear\Helpers\HTML;
use DeBear\Helpers\Resources;
use DeBear\Models\HollyAndMax\Purchases;
use DeBear\Repositories\Time;

class GiftList extends Controller
{
    /**
     * Display the Gift List
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Custom page config.
        HTML::noHeaderLink();
        HTML::setMetaDescription(FrameworkConfig::get('debear.links.home.descrip')); // As the home link is disabled.
        Resources::addCSS('gift_list.css');
        Resources::addJS('gift_list.js');

        // Get all the outstanding gifts.
        $gifts = DB::connection('mysql_www')
            ->table('HOLLYANDMAX_GIFTS')
            ->selectRaw(
                'HOLLYANDMAX_GIFTS.*,
                 CONCAT(HOLLYANDMAX_GIFTS.image_domain, HOLLYANDMAX_GIFTS.image_src) AS image,
                 HOLLYANDMAX_GIFTS.image_domain,
                 HOLLYANDMAX_STORES.name AS store_name,
                 HOLLYANDMAX_STORES.logo AS store_logo,
                 HOLLYANDMAX_CATEGORIES.cat_name,
                 HOLLYANDMAX_CATEGORIES.cat_descrip,
                 HOLLYANDMAX_GIFTS.quantity
                   - SUM(IFNULL(HOLLYANDMAX_PURCHASES.quantity, 0)) AS quantity_remaining'
            )
            ->join('HOLLYANDMAX_STORES', 'HOLLYANDMAX_STORES.store_id', '=', 'HOLLYANDMAX_GIFTS.store_id')
            ->join('HOLLYANDMAX_CATEGORIES', 'HOLLYANDMAX_CATEGORIES.cat_id', '=', 'HOLLYANDMAX_GIFTS.cat_id')
            ->leftJoin('HOLLYANDMAX_PURCHASES', 'HOLLYANDMAX_PURCHASES.gift_id', '=', 'HOLLYANDMAX_GIFTS.gift_id')
            ->groupBy('HOLLYANDMAX_GIFTS.gift_id')
            ->having('quantity_remaining', '>', '0')
            ->orderBy('HOLLYANDMAX_GIFTS.name')
            ->get();
        $cats = array_unique(array_column($gifts->toArray(), 'cat_name'));
        $stores = array_unique(array_column($gifts->toArray(), 'store_name'));
        sort($cats);
        sort($stores);

        // Tweak the CSP to account for the off-site images.
        $csp_dom = array_merge(
            FrameworkConfig::get('debear.headers.csp.policies.img-src'),
            array_unique(array_column($gifts->toArray(), 'image_domain'))
        );
        FrameworkConfig::set(['debear.headers.csp.policies.img-src' => $csp_dom]);

        // Process the template.
        return response(view('hollyandmax/list', compact(['gifts', 'cats', 'stores'])));
    }

    /**
     * Record a gift purchase
     * @param integer $gift_id ID of the gift to lookup.
     * @return \Illuminate\Http\Response
     */
    public function store(int $gift_id)
    {
        // Validate the gift argument.
        $gift = DB::connection('mysql_www')
            ->table('HOLLYANDMAX_GIFTS')
            ->selectRaw(
                'HOLLYANDMAX_GIFTS.*,
                 SUM(IFNULL(HOLLYANDMAX_PURCHASES.quantity, 0)) AS quantity_purchased'
            )
            ->leftJoin('HOLLYANDMAX_PURCHASES', 'HOLLYANDMAX_PURCHASES.gift_id', '=', 'HOLLYANDMAX_GIFTS.gift_id')
            ->where('HOLLYANDMAX_GIFTS.gift_id', '=', $gift_id)
            ->groupBy('HOLLYANDMAX_GIFTS.gift_id')
            ->orderBy('HOLLYANDMAX_GIFTS.name')
            ->get();
        if (!$gift->count() || ($gift->first()->quantity_purchased >= $gift->first()->quantity)) {
            return response('0::Invalid Arguments', 400)
                    ->header('Content-Type', 'text/plain');
        }

        // Create.
        $purchase = new Purchases(
            [
                'gift_id' => $gift_id,
                'quantity' => 1,
                'when_done' => App::make(Time::class)->format(),
                'agent_ip' => Request::server('REMOTE_ADDR'),
                'agent_browser' => Request::server('HTTP_USER_AGENT'),
            ]
        );
        $purchase->save();

        // Return a valid response.
        return response('1::OK', 200)
                ->header('Content-Type', 'text/plain');
    }
}
