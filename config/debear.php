<?php

    return [
        /*
         * Site name details
         */
        'names' => [
            'site' => 'Holly &amp; Max',
            'section' => 'Holly &amp; Max Gift List',
        ],

        /*
         * Site-specific resource additions
         */
        'resources' => [
            'css' => [
                'layout.css',
            ],
        ],

        /*
         * Various Nav/Header/Footer/Sitemap links
         */
        'links' => [
            'login' => [
                'enabled' => false,
            ],
            'register' => [
                'enabled' => false,
            ],
            'logout' => [
                'enabled' => false,
            ],
            'home' => [
                'enabled' => false,
                'descrip' => 'The Gift List for Holly &amp; Max',
            ],
            'sitemap' => [
                'enabled' => false,
            ],
        ],

        /*
         * Site revision info
         */
        'version' => [
            'breakdown' => [
                'major' => 3,
                'minor' => 0,
            ],
        ],

        /*
         * Google Analytics
         */
        'analytics' => [
            'urchins' => [
                'tracking' => [ 'UA-48789782-11' ],
            ],
        ],
    ];
