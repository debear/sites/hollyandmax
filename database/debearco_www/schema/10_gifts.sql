CREATE TABLE `HOLLYANDMAX_GIFTS` (
  `gift_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) COLLATE latin1_general_ci NOT NULL,
  `description` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  `notes` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  `image_domain` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `image_src` VARCHAR(200) COLLATE latin1_general_ci DEFAULT NULL,
  `store_id` TINYINT(3) UNSIGNED NOT NULL,
  `product_code` VARCHAR(20) COLLATE latin1_general_ci NOT NULL,
  `cat_id` TINYINT(3) UNSIGNED NOT NULL,
  `price` VARCHAR(10) COLLATE latin1_general_ci DEFAULT NULL,
  `info_url` VARCHAR(255) COLLATE latin1_general_ci DEFAULT NULL,
  `quantity` TINYINT(3) UNSIGNED NOT NULL,
  `star_item` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`gift_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE latin1_general_ci;

CREATE TABLE `HOLLYANDMAX_CATEGORIES` (
  `cat_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cat_name` VARCHAR(50) COLLATE latin1_general_ci NOT NULL,
  `cat_descrip` TEXT COLLATE latin1_general_ci DEFAULT NULL,
  `cat_order` TINYINT(3) UNSIGNED NOT NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE latin1_general_ci;

CREATE TABLE `HOLLYANDMAX_STORES` (
  `store_id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(75) COLLATE latin1_general_ci NOT NULL,
  `logo` VARCHAR(200) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE latin1_general_ci;
