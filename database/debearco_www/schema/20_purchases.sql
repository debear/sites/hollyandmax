CREATE TABLE `HOLLYANDMAX_PURCHASES` (
  `purchase_id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gift_id` TINYINT(3) UNSIGNED NOT NULL,
  `purchaser` VARCHAR(100) COLLATE latin1_general_ci DEFAULT NULL,
  `quantity` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `when_done` DATETIME NOT NULL,
  `agent_ip` VARCHAR(15) COLLATE latin1_general_ci NOT NULL,
  `agent_browser` VARCHAR(255) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE latin1_general_ci;
