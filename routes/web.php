<?php

/**
 * Routes for the Holly and Max sub-domain
 */

// Main Page.
Route::get('/', 'HollyAndMax\GiftList@index');

// Gift Reserved.
Route::post('/gift/{gift_id}/reserve', 'HollyAndMax\GiftList@store')
        ->where('gift_id', '[0-9]+');
